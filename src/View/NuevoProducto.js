import React, {useState} from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { mainListItems, secondaryListItems } from '../components/listItems';
import logo from '../../src/assets/logo1.png'
import DropMenu from '../components/DropMenu'
import Button from '@material-ui/core/Button';
import Swal from 'sweetalert2'
import TextField from '@material-ui/core/TextField';
import DropdownCategory from '../components/NuevoProducto/DropdownProductCategory'
import DropdownPan from '../components/NuevoProducto/DropdownProductPan'
import DropdownCarne from '../components/NuevoProducto/DropDownProductCarne'
import DropdownTomate from '../components/NuevoProducto/DropDownProductTomate'
import DropdownQueso from '../components/NuevoProducto/DropDownProductQueso'
import DropdownLechuga from '../components/NuevoProducto/DropDownProductLechuga'
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        GAVO
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
}));

const logos = {
  background: `url(${logo}) no-repeat scroll center 3% / contain`,
  backgroundColor: 'black'
}

export default function Dashboard() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  
  const [name, setName] = useState('')
  const [price, setPrice] = useState('')
  const [description, setDescription] = useState('')
  const [imageUrl, setImageUrl] = useState('')
  
  const [ingredients, setIngredients] = useState('')
  const [pan, setPan] = useState('')
  

  const handleNameChange = (event) => setName(event.target.value)
  const handlePriceChange = (event) => setPrice(event.target.value)
  const handleDescriptionChange = (event) => setDescription(event.target.value)
  const handleImageUrlChange = (event) => setImageUrl(event.target.value)
  const handleIngredientsChange = (event) => setIngredients(event.target.value)
  const handlePanChange = (event) => setPan(event.target.value)
  

  const [category, setCategory] = React.useState('');

  const handleChange = (event) => {
    setCategory(event.target.value);
  };

  async function login() {
    await fetch('http://localhost:5000/api/pro/create', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({name, price, description, imageUrl, category, ingredients})
    })
    .then(function (result) {
      if(result['ok'] === true){
        result.text().then(function(data) {
          
          Swal.fire({
            icon: 'success',
            title: '¡NICE"',
            text: data,
            timer: 3500
        })
        })
        }
        else{
          result.text().then(function(data) { 
            Swal.fire({
              icon: 'error',
              title: '¡UAGN!',
              text: data,
              timer: 3500
          })
          })
          }
        
    })
    .catch (function (error) {
      console.log(error)
        Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: error,
        timer: 3500
      })
    });
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" style={logos} className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar} >
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            
          </Typography> 
          <IconButton color="inherit" >  
              <DropMenu />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Drawer
      
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon} style={{backgroundColor: 'black'}}>
          <IconButton onClick={handleDrawerClose} style={{color:'yellow'}}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        
        <List style={{backgroundColor:'black'}}>{mainListItems}</List>
        <List style={{backgroundColor:'black'}}>{secondaryListItems}</List>
      </Drawer>
      <main className={classes.content}  >
            <div className={classes.appBarSpacer} />
            <Container className={classes.container} >
            <div className={classes.paper}>
            <Typography component="h1" variant="h5">
                ¡Registra un nuevo producto!
            </Typography>
            <form className={classes.form} noValidate>
            <br/><br/>
              <TextField
                  value={name}
                  onChange={handleNameChange}
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="name"
                  label="Name"
                  name="name"
                  autoComplete="name"
                  autoFocus
              />
              <TextField
                  value={price}
                  onChange={handlePriceChange}
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="price"
                  label="Price"
                  name="price"
                  autoComplete="price"
                  autoFocus
              />
              <TextField
                value={description}
                onChange={handleDescriptionChange}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="description"
                label="Description"
                name="description"
                autoComplete="description"
                autoFocus
              />
              <TextField
                value={imageUrl}
                onChange={handleImageUrlChange}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="imageUrl"
                label="ImageUrl"
                name="imageUrl"
                autoComplete="imageUrl"
                autoFocus
              />
                <div>
              <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">Category</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={category}
                  onChange={handleChange}
                >
                  <MenuItem value={"5e8e712f7e473826777bb492"}>Hamburguesa</MenuItem>
                  <MenuItem value={"5e9321f44eef1c0a1361ce41"}>PAPAPIZA</MenuItem>
                  <MenuItem value={"5e9322774eef1c0a1361ce43"}>BEBIDA</MenuItem>
                </Select>
              </FormControl>
            </div>
                <br/>
                <TextField
                  value={ingredients}
                  onChange={handleIngredientsChange}
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="ingredients"
                  label="Ingredients"
                  name="ingredients"
                  autoComplete="ingredients"
                  autoFocus
                />
                <br/>
                <br/>
                <Button
                fullWidth
                style={{marginTop:20}}
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={() => login()}
                >
                Registrar nuevo Producto
                </Button>
            </form>
            </div>
          <Box pt={0}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}