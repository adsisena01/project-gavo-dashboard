import React, {useState} from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { mainListItems, secondaryListItems } from '../components/listItems';
import logo from '../../src/assets/logo1.png'
import DropMenu from '../components/DropMenu'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Swal from 'sweetalert2'
import axios from 'axios'
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';



function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        GAVO
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
}));

const logos = {
  background: `url(${logo}) no-repeat scroll center 3% / contain`,
  backgroundColor: 'black'
}

export default function Dashboard() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  const [quantity, setQuantity] = useState('')
  const [insumoId, setInsumoId] = useState('')
  

  const handleQuantityChange = (event) => setQuantity(event.target.value)
  const handleInsumoIdChange = (event) => setInsumoId(event.target.value)
  

  

  async function login() {
    await fetch('http://localhost:5000/api/stock/create', {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        
      },
      body: JSON.stringify({insumoId})
    })
    .then(function (result) {
      if(result['ok'] === true){
        result.text().then(function(data) {
          
          Swal.fire({
            icon: 'success',
            title: '¡NICE"',
            text: data,
            timer: 3500
        })
        })
        }
        else{
          result.text().then(function(data) { 
            Swal.fire({
              icon: 'error',
              title: '¡UAGN!',
              text: data,
              timer: 3500
          })
          })
          }
        
    })
    .catch (function (error) {
      console.log(error)
        Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: error,
        timer: 3500
      })
    });
  }

  let stockId = [];
    axios
      .get("http://localhost:5000/api/stock/list")
      .then(res => {
        console.log(res.data.data.data);
          stockId.push((res.data.data));  
    })

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" style={logos} className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar} >
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            
          </Typography>
          <IconButton color="inherit" >  
              <DropMenu />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Drawer
      
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon} style={{backgroundColor: 'black'}}>
          <IconButton onClick={handleDrawerClose} style={{color:'yellow'}}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        
        <List style={{backgroundColor:'black'}}>{mainListItems}</List>
        <List style={{backgroundColor:'black'}}>{secondaryListItems}</List>
      </Drawer>
      <main className={classes.content}  >
            <div className={classes.appBarSpacer} />
            <Container className={classes.container} >
            <div className={classes.paper}>
            <Typography component="h1" variant="h5">
                ¡Editar cantidad del insumo!
            </Typography>
            <form className={classes.form} noValidate>
            <div>
              <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">Category</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={insumoId}
                  onChange={handleInsumoIdChange}
                >
                  <MenuItem value={stockId._id}>{stockId.name}</MenuItem>
                </Select>
              </FormControl>
            </div>
                <br/>
                <Button
                fullWidth
                style={{marginTop:20}}
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={() => login()}
                >
                Registrar insumo
                </Button>
            </form>
            </div>
          <Box pt={4}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}