import React, { Component } from 'react';
import { BrowserRouter as Router, Redirect, Switch, Route } from 'react-router-dom';
import Dashboard from '../src/View/Dashboard'
import Orders from '../src/View/Orders'
import Users from '../src/View/Users'
import Balance from '../src/View/Balance'
import Colaboradores from '../src/View/Colaboradores'
import Login from '../src/View/Login'
import NewEmployee from './View/NewEmployee'
import NuevoProducto from './View/NuevoProducto'
import NuevoInsumo from './View/NuevoInsumo'
import NewCollaborator from './View/NewCollaborator'
import ModificarCantidadInsumo from './View/ModifyCantidadInsumo'
import decode from 'jwt-decode';


    const isAuthenticated = () => {
        const token = localStorage.getItem('token')
        let isValid = true
        try{
            isValid = decode(token);
            console.log(isValid)
        }catch(e){
            return false;
        }
        return isValid;

      };

    const MyRoute = (props)=>(
        isAuthenticated()
        ?<Route {...props} />
        :<Redirect to="/login" />
    )

    export default () => ( 

        <Router>
            <Switch>
                <MyRoute path='/' exact component={Dashboard} />
                <Route path='/login' exact component={Login} />
                <MyRoute path='/orders' exact component={Orders} />
                <MyRoute path='/users' exact component={Users} />
                <MyRoute path='/balance' exact component={Balance} />
                <MyRoute path='/colaboradores' exact component={Colaboradores} />
                <MyRoute path='/nuevoempleado' exact component={NewEmployee} />
                <MyRoute path='/nuevoproducto' exact component={NuevoProducto} />
                <MyRoute path='/nuevoinsumo' exact component={NuevoInsumo} />
                <MyRoute path='/nuevocolaborador' exact component={NewCollaborator} />
                <MyRoute path='/modificarcantidadinsumo' exact component={ModificarCantidadInsumo} />
            </Switch>
        </Router>

    );