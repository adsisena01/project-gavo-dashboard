import React, { Component } from 'react';
import { Grommet } from 'grommet';
import { grommet } from 'grommet';
import { Box } from 'grommet';
import { DataTable } from 'grommet';
import Axios from 'axios';

const columns = [
  {
    property: 'date',
    header: 'Date'
  },
  {
    property: 'user',
    header: 'User',
  },
  {
    property: 'shipTo',
    header: 'Ship To',
  },
  {
    property: 'paymentMethod',
    header: 'Payment Method',
  },
  {
    property: 'amount',
    header: 'Amount',
  },
];

            
        

class SimpleDataTable extends Component{

    state = {
        orders: []
    }       

    async componentDidMount(){
        const res = await Axios.get('http://localhost:3000/orders')
        this.setState({orders: res.data})
        console.log(this.state.orders)
    }
    
       
    render(){
        return (
            <Grommet theme={grommet}>
                <Box align='center' pad='large'>
                <DataTable columns={columns} data={this.state.orders} />
                
                </Box>
            </Grommet>
        )
    }
      
}

export default SimpleDataTable