import React, { useState, useEffect } from "react";
import { Line } from "react-chartjs-2";
import axios from "axios";

const Dankmemes = () => {
  const [chartData, setChartData] = useState({});
  const [employeeSalary, setEmployeeSalary] = useState([]);
  const [employeeAge, setEmployeeAge] = useState([]);

  const chart = () => {
    let empSal = [];
    let empAge = [];
    axios
      .get("http://localhost:5000/api/order/list")
      .then(res => {
        console.log(res);
        for (const dataObj of res.data) {
          empSal.push((dataObj.amount));
          empAge.push((dataObj.createdAt));
        }
        setChartData({
          labels: empAge.map( item => Intl.DateTimeFormat('es-CO', {month: 'long', day:'numeric'}).format(new Date(item))),
          datasets: [
            {
              label: "Ultimas ventas realizadas",
              data: empSal,
              backgroundColor: ["rgba(75, 192, 192, 0.6)"],
              borderWidth: 4
            }
          ]
        });
      })
      .catch(err => {
        console.log(err);
      });
    console.log(empSal, empAge);
  };

  useEffect(() => {
    chart();
  }, []);
  return (
    <div className="App">
      <div style={{marginLeft:'10%'}}>
        <Line
          width="1250%"
          height="700%"
          data={chartData}
          options={{
            responsive: true,
            title: { text: "Ultimos ingresos", display: true },

          }}
        />
      </div>
    </div>
  );
};

export default Dankmemes;