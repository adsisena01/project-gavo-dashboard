import React, {Component} from 'react';
import Typography from '@material-ui/core/Typography';
import Avatar from '../components/Avatar';
import Modal from '../components/Modal'
import Axios from 'axios'
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';


class Insumos extends Component {

  state = {
    insumos:[]
  }

  async componentDidMount(){
        const res = await Axios.get('http://localhost:5000/api/stock/list')
        this.setState({insumos: res.data.data})
        console.log(this.state.insumos)
    }

  render(){
  return (
    <Grid container spacing={1} style={{marginTop:15}} justify="center" >
      {this.state.insumos.map(insumo => (
        <Grid item xs={12}  lg={2} md={12} spacing={6} alignItems="center" justify="center" style={{marginBottom:20}}>
          <Paper style={{width:170,height:150}} >
            <div style={{float:'left'}}>
            <Avatar imageURI={insumo.imageUrl} />
            </div>
            <Typography component="p" variant="h6" style={{textAlign:'center'}} >
              {insumo.name}
            </Typography>
            <Typography color="textSecondary" style={{textAlign:'center'}}>
              {insumo.quantity}
            </Typography>
          </Paper>
      </Grid>
      ))}
    </Grid>
  );
  }
}

export default Insumos