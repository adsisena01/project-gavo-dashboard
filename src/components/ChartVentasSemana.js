import React, { Component } from 'react';
import { Col } from 'react-flexbox-grid';
import {Typography} from '@material-ui/core'
import {  Line} from 'react-chartjs-2'
import { Card } from 'antd';

class Chart extends Component {

    arrayPepitos=['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo']
    arrayDatosPepitos=[
        4,
        3,
        7,
        2,
        4,
        6,
        10
    ]

    constructor(props) {
        super(props);

        this.state = { 
            chartData: {
                labels:this.arrayPepitos,
                datasets:[
                    {
                        label:'Ventas Semana',
                        data:this.arrayDatosPepitos,
                        backgroundColor: [
                        'rgba(204,209,209)',
                        'rgba(52,73,94)',
                        'rgba(249,231,159)',

                        ]
                        
                        
                    }]
            }
         }
    }

    render() { 
        return ( 
            <div className="chart">
                    <Col xs={12} sm={12} md={12} lg={12} >
                    <Typography variant="h5" component="h5" style={{marginLeft:70, marginTop:30}}>
                        VENTAS POR ESTA SEMANA
                    </Typography>
                    <div className="site-card-border-less-wrapper" alignSelf="center">
                        <Card bordered={false} alignSelf="center" style={{ width: '150%', marginLeft:70, marginTop:20 }}>
                        <Line
                            data={this.state.chartData}
                            options={{
                                maintainAspectRatio: true
                            }}>
                        </Line>
                        </Card>
                    </div>                    
                    </Col>
            </div>
        );
    }
}
 
export default Chart;
