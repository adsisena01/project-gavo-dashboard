import React, { Component } from 'react'
import { Dropdown } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css'
import Axios from 'axios'




class DropdownExampleSearchSelection extends Component{
    
  state= {
    key: []
  }

    async componentDidMount(){
        const res = await Axios.get('http://localhost:5000/api/stock/details/5e8731d202f37e74c366a4f0')
        this.setState({key: res.data.data})
    }

    


render(){
    const countryOptions = [
        { key: this.state.key.custom_id, value: this.state.key._id, text: this.state.key.name },
      ]    
    return(
   <Dropdown
    placeholder='Selecionar Tipo de Carne'
    fluid
    search
    selection
    options={countryOptions}
  />
)
}
}

export default DropdownExampleSearchSelection
