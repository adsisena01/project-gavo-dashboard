import React, { Component } from 'react'
import { Dropdown } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';
import Axios from 'axios'




class DropdownExampleSearchSelection extends Component{
    
  state= {
    key: []
  }

    async componentDidMount(){
        const res = await Axios.get('http://localhost:5000/api/stock/details/5e8e66f3e09a6d55c7182fcd')
        this.setState({key: res.data.data})
    }

    


render(){
    const countryOptions = [
        { key: 'pan_01', value: '5e8e66f3e09a6d55c7182fcd', text: 'PAN AJONJOLI' },
      ]    
    return(
   <Dropdown
    placeholder='Selecionar Tipo de Pan'
    fluid
    search
    selection
    options={countryOptions}
  />
)
}
}

export default DropdownExampleSearchSelection
