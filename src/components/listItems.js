import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import PeopleIcon from '@material-ui/icons/People';
import BarChartIcon from '@material-ui/icons/BarChart';
import LayersIcon from '@material-ui/icons/Layers';
import AssignmentIcon from '@material-ui/icons/Assignment';
import { Link } from 'react-router-dom';

export const mainListItems = (
  <div >
    <ListItem button >
      <ListItemIcon>
        <DashboardIcon style={{color:'#FFCC33'}} />
      </ListItemIcon>
      <Link to="/" className="link" style={{ color:'#FFCC33', padding: '14px 25px', textAlign:'center', textDecoration:'none', display:'inline-block'}}>Inicio</Link>
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <ShoppingCartIcon style={{color:'#FFCC33'}} />
      </ListItemIcon>
      <Link to="/orders" className="link" style={{ color:'#FFCC33', padding: '14px 25px', textAlign:'center', textDecoration:'none', display:'inline-block'}}>Ordenes</Link>
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <PeopleIcon style={{color:'#FFCC33'}}/>
      </ListItemIcon>
      <Link to="/users" className="link" style={{ color:'#FFCC33', padding: '14px 25px', textAlign:'center', textDecoration:'none', display:'inline-block'}}>Usuarios</Link>
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <BarChartIcon style={{color:'#FFCC33'}}/>
      </ListItemIcon>
      <Link to="/balance" className="link" style={{ color:'#FFCC33', padding: '14px 25px', textAlign:'center', textDecoration:'none', display:'inline-block'}}>Balance</Link>
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <LayersIcon style={{color:'#FFCC33'}}/>
      </ListItemIcon>
      <Link to="/colaboradores" className="link" style={{ color:'#FFCC33', padding: '14px 25px', textAlign:'center', textDecoration:'none', display:'inline-block'}}>Colaboradores</Link>
    </ListItem>
    <ListItem button>
      <Link to="/colaboradores" className="link" style={{ color:'#FFCC33', padding: '14px 25px', textAlign:'center', textDecoration:'none', display:'inline-block'}}></Link>
    </ListItem>
    <ListItem button>
      <Link to="/colaboradores" className="link" style={{ color:'#FFCC33', padding: '14px 25px', textAlign:'center', textDecoration:'none', display:'inline-block'}}></Link>
    </ListItem>
  </div>
);

export const secondaryListItems = (
  <div>
    <ListSubheader inset>Saved reports</ListSubheader>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText  />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText  />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText  />
    </ListItem>
  </div>
);