import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';


const useStyles = makeStyles(theme => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

export default function FormPropsTextFields(props) {
  const classes = useStyles();

  return (
    <form className={classes.root} noValidate autoComplete="off">
        <Typography component="p" variant="h6">
        {props.titlemodal}
      </Typography><br/>
      <TextField
          disabled
          id="outlined-disabled"
          label={props.disabled}
          variant="outlined"
        />
      <div>
        <TextField
          required
          id="outlined-required"
          label={props.und}
          variant="outlined"
        /> <br/>
      </div>
      <div style={{marginLeft:'70%'}}>
        <Button>¡AGREGAR!</Button>
      </div>
    </form>
  );
}