import React, { Component } from 'react';
import {Button} from '@material-ui/core'
import MUIDataTable from "mui-datatables";
import Axios from 'axios'

const columns = ["name", "phone1", "phone2", "email", "address"];


  const options = {
    filterType: 'checkbox',
  };

class Table extends Component {

  state = {
    products: []
  }       

  async componentDidMount(){
      const res = await Axios.get('http://localhost:5000/api/collaborator/list')
      this.setState({products: res.data.data})
      console.log(this.state.products)
  }

render(){
  return(
    <div>
      <div style={{marginLeft:'75%'}}>
        <Button href="/nuevocolaborador">¡Añadir nuevo colaborador!</Button>
      </div>
        <MUIDataTable
          title={"Lista de Colaboradores"}
          data={this.state.products}
          columns={columns}
          options={options}
        />
    </div>
  )
  }
}

export default Table