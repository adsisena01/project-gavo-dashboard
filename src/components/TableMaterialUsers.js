import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import Axios from 'axios'

const columns = ["name", "role", "email", "username"];


  const options = {
    filterType: 'checkbox',
  };

class Table extends Component {

  state = {
    users: []
  }       

  async componentDidMount(){
      const res = await Axios.get('http://localhost:5000/api/users/list-users')
      this.setState({users: res.data})
      console.log(this.state.users)
  }

render(){
  return(
    <MUIDataTable
      title={"Lista de usuarios registrados"}
      data={this.state.users}
      columns={columns}
      options={options}
    />
  )
  }
}

export default Table