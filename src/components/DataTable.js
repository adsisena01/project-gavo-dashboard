import React, { Component } from 'react';
import { Grommet } from 'grommet';
import { grommet } from 'grommet';
import { Box } from 'grommet';
import { DataTable } from 'grommet';
import Axios from 'axios';

const columns = [
  {
    property: 'name',
    header: 'Name'
  },
  {
    property: 'shortDescription',
    header: 'Description',
  },
  {
    property: 'price',
    header: 'Price',
  },
  {
    property: 'category',
    header: 'Category',
  },
  {
    property: 'stock',
    header: 'Stock',
  },
];

            
        

class SimpleDataTable extends Component{

    state = {
        products: []
    }       

    async componentDidMount(){
        const res = await Axios.get('http://localhost:3000/products')
        this.setState({products: res.data})
        console.log(this.state.products)
    }
    
       
    render(){
        return (
            <Grommet theme={grommet}>
                <Box align='center' pad='large'>
                <DataTable columns={columns} data={this.state.products} />
                
                </Box>
            </Grommet>
        )
    }
      
}

export default SimpleDataTable