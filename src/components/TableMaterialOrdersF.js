import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import Axios from 'axios'

const columns = ["status", "transaction_id", "amount", "user.name"];


  const options = {
    filterType: 'checkbox',
  };

class Table extends Component {

  state = {
    orders: []
  }       

  async componentDidMount(){
      const res = await Axios.get('http://localhost:5000/api/order/list')
      this.setState({orders: res.data})
      console.log(this.state.orders)
  }

render(){
  return(
    <MUIDataTable
      title={"Lista de Ordenes FINALIZADAS"}
      data={this.state.orders}
      columns={columns}
      options={options}
    />
  )
  }
}

export default Table