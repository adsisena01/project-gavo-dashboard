import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import Axios from 'axios'

const columns = ["name", "document", "phone", "email", "eps"];


  const options = {
    filterType: 'checkbox',
  };

class Table extends Component {

  state = {
    users: []
  }       

  async componentDidMount(){
      const res = await Axios.get('http://localhost:5000/api/employees/list')
      this.setState({users: res.data.data})
  }

render(){
  return(
    <MUIDataTable
      title={"Lista de Empleados"}
      data={this.state.users}
      columns={columns}
      options={options}
    />
  )
  }
}

export default Table